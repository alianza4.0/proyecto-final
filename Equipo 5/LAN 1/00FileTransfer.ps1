﻿# Tranf de archivos
$VMmachine = "IIS_LAN-A"
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force
$user = "MODULOA\ISS" # Nombre del usuario de la máquina cliente VM
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, $pass
$sesion = New-PSSession -VMName $VMmachine -Credential $credential
#Paths
$origen = "C:\prueba.txt"
$destino = "C:\copia.txt"

#Para copiar un archivo
copy -Path $origen -Destination $destino -ToSession $sesion
#Para copiar una carpeta
copy -Recurse -Path $origen -Destination $destino -ToSession $sesion
#