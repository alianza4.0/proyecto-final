﻿# Variables
$ipaddress = "10.5.0.4"
$dnsaddress = "10.5.0.3"
$machineName = "VDIA"
$interfaz = Get-NetAdapter
$interfaz = $interfaz.Name

# Desactivar Firewall
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
netsh advfirewall Set allprofiles State Off 

# Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

#Configuración de IP y DNS 
Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 27
Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

#Configuración de nombre de pc
Rename-Computer –Newname $machineName -Restart