﻿#Nombres de máquinas
$IIS = "IIS_LAN-A"
$VDI = "VDI_LAN-A"
$CAsub = "CAsub_LAN-A"
$DHCP = "DHCP_LAN-A"
$AD = "AD_LAN-A"
$Win10 = "Win10_LAN-A"

#contraseña predilecta
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

#===============================Actualiza credenciales===============================================0
# Nombres nuevos post AD
$userIISdominio = "MODULOA\ISS" # Nombre del usuario de la máquina cliente VM
$userVDIdominio = "MODULOA\VDI" # Nombre del usuario de la máquina server VM
$userCAsubdominio = "MODULOA\CAsub" # Nombre del usuario de la máquina server VM
$userDHCPdominio = "MODULOA\DHCP" # Nombre del usuario de la máquina server VM
$userADdominio = "MODULOA\AD" # Nombre del usuario de la máquina server VM
$userWin10dominio = "MODULOA\Win10" # Nombre del usuario de la máquina server VM

#Creamos las nuevas credenciales post AD
$credentialiis = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userIISdominio, $pass
$credentialvdi = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userVDIdominio, $pass
$credentialcasub = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userCAsubdominio, $pass
$credentialdhcp = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userDHCPdominio, $pass
$credentialad = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userADdominio, $pass
$credentialwin10 = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userWin10dominio, $pass

# Creacion de nuevas sesiones
$sesionIIS = New-PSSession -VMName $IIS -Credential $credentialiis
$sesionVDI = New-PSSession -VMName $VDI -Credential $credentialvdi
$sesionCAsub = New-PSSession -VMName $VDI -Credential $credentialcasub
$sesionDHCP = New-PSSession -VMName $VDI -Credential $credentialdhcp
$sesionAD = New-PSSession -VMName $VDI -Credential $credentialad
$sesionWin10 = New-PSSession -VMName $VDI -Credential $credentialwin10

# Ingresar a sesion
Enter-PSSession -Session $sesionIIS
#Enter-PSSession -Session $sesionVDI
#Enter-PSSession -Session $sesionCAsub
#Enter-PSSession -Session $sesionDHCP
#Enter-PSSession -Session $sesionAD
#Enter-PSSession -Session $sesionWin10