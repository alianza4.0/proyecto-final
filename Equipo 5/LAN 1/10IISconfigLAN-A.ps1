﻿#***************Importante**************
#Ejecutar el siguiente comando en el DC del AD
$nombreURL = "IIS-VMM"
$dirIIS = "10.5.0.7"
Add-DnsServerResourceRecordA -Name $nombreURL -ZoneName "ModuloA.local" -AllowUpdateAny -IPv4Address $dirIIS


#En el server de IIS
#Instalamos roles
Import-Module ServerManager
Install-WindowsFeature -Name Web-Mgmt-Console, web-server, web-webserver, web-common-http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Http-Redirect, Web-DAV-Publishing, Web-Health, Web-Http-Logging, Web-Performance, Web-Stat-Compression, Web-Mgmt-Tools, Web-Mgmt-Console, Web-Security, Web-Filtering, Web-Basic-Auth, Web-CertProvider, Web-Client-Auth, Web-Digest-Auth, Web-Cert-Auth, Web-IP-Security, Web-Url-Auth, Web-Windows-Auth, Web-Scripting-Tools
Install-WindowsFeature -Name FS-Resource-Manager -IncludeManagementTools
Import-Module WebAdministration


# IMPORTANTE!!! - Nos movemos a la carpeta de IIS
cd IIS:\

#Configuración de IIS
$rutaDelSitio = "C:\inetpub\sitio"
$appPool = "AppLanA"                  
$NombreDelSitio = "IIS-VMM"                        
$NombreCompletoSitio = "IIS-VMM.ModuloA.local"            

New-Item $rutaDelSitio -type Directory
Set-Content $rutaDelSitio\Default.htm "<h1>Welcome LAN A Site</h1>"
New-Item IIS:\AppPools\$appPool
New-Item IIS:\Sites\$NombreDelSitio -physicalPath $rutaDelSitio -bindings @{protocol="http";bindingInformation=":80:"+$NombreCompletoSitio} 
Set-ItemProperty IIS:\Sites\$NombreDelSitio -name applicationPool -value $appPool
Start-Website -Name $NombreDelSitio

cd C:\Users