﻿#+++ Configuracion Acceso Servers AD********************************************************

# Nombres de las máquinas
$IIS = "IIS_LAN-A"
$VDI = "VDI_LAN-A"
$CAsub = "CAsub_LAN-A"
$DHCP = "DHCP_LAN-A"
$AD = "AD_LAN-A"
$Win10 = "Win10_LAN-A"

# Nombres de usuarios

# Default de servers
$defaultAdmin = "Administrator" #Nombre del usuario default en servers
$usuarioWin10 = "Win10" #Nombre del usuario creado en Windows 10

#Contraseña
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force #Contraseña predilecta

# Creacion de credenciales por Virtual Machine Hyper-V pre config (preAD)
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $defaultAdmin, $pass
$credentialWin10 = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $usuarioWin10, $pass

# Creacion de sesiones
$sesionIIS = New-PSSession -VMName $IIS -Credential $credential
$sesionVDI = New-PSSession -VMName $VDI -Credential $credential
$sesionCAsub = New-PSSession -VMName $CAsub -Credential $credential
$sesionDHCP = New-PSSession -VMName $DHCP -Credential $credential
$sesionAD = New-PSSession -VMName $AD -Credential $credential
$sesionWin10 = New-PSSession -VMName $Win10 -Credential $credentialWin10

# Ingresar a sesion
#Solo cabiar la sesion a la que se quieren unir
# Del padre pre AD
Enter-PSSession -Session $sesionIIS
#Enter-PSSession -Session $sesionVDI
#Enter-PSSession -Session $sesionCAsub
#Enter-PSSession -Session $sesionDHCP
#Enter-PSSession -Session $sesionAD
#Enter-PSSession -Session $sesionWin10